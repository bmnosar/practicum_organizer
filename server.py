import sys
reload(sys)
sys.setdefaultencoding("UTF8")
import os
from flask import *
from flask_socketio import SocketIO, emit
import psycopg2
import psycopg2.extras
from collections import OrderedDict, defaultdict
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from db import *
import report as rp
import uuid
import string
import random
# import ast
import shutil

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

app = Flask(__name__, static_folder='static')
socketio = SocketIO(app)
app.secret_key= os.urandom(24).encode('hex')

globalDict = {'accessCode': ''}

#######################################
# Routes to webpages

@app.route('/')
def mainIndex():
    return render_template('index.html', currentPage='home')
    
@app.route('/student', methods=['GET'])
def getStudentData():
    return render_template('student_form.html')

@app.route('/teacher', methods=['GET'])
def getTeacherData():
    return render_template('teacher_form.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    return render_template('login.html')
    
@app.route('/logout', methods=['GET', 'POST'])
def logout():
    session.clear()
    flash('You have successfully logged out!')
    return redirect(url_for('login'))

@app.route('/practica', methods=['GET', 'POST'])
def practica():
    loginQuery = "SELECT passwordid FROM login WHERE password = crypt(%s, password)"
    hasRedirect = False
    error = ''
    
    if request.method == "POST":
        if 'password' in request.form:
            pd = request.form['password']
            hasRedirect = True

        results = select_query_db(loginQuery, (pd, ), True)
        
        if not results:
            error += 'Incorrect Password.\n'
            flash('Invalid Password')
            hasRedirect = False
            
        else:
            session['userid'] = results['passwordid']
            session['loggedIn'] = True

        if hasRedirect:
            return render_template('practicum_assignment.html')
        else:    
            return redirect(url_for('login'))
        
    else:
        if session and session['loggedIn']:
            return render_template('practicum_assignment.html')
        else:
            flash('Please log in to access practica')
            return redirect(url_for('login'))
        

#################################################
# Reset Password functions

@socketio.on('forgotPassword', namespace='/login') 
def forgotPassword():
    
    chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
    accessCode = ''.join(random.SystemRandom().choice(chars) for _ in range(10))
    globalDict['accessCode'] = accessCode
    
    TEXT = "A password reset has been requested for the Practicum Organizer application.\n\n" + \
    "If you did not make this request, you can ignore this email. This password reset can only be made by those who have access to the login site." + \
    "It does not indicate that the application is in any danger of being accessed by someone else.\n\n" + \
    "The access code to reset your password is: " + accessCode + "\n\nThank you."
    SUBJECT = '[Practicum Organizer] Reset password'
    From = 'practicumorganizer@gmail.com'
    To = 'sheldonmcclung@gmail.com' #Just to test
    
    try:
        smtpObj = smtplib.SMTP("smtp.gmail.com", 587)
        #smtpObj = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        #server.set_debuglevel(1)
        #smtpObj.ehlo()
	smtpObj.starttls()
	#smtpObj.ehlo()
        smtpObj.login('practicumorganizer@gmail.com', 'Grown Jacob Broom Spar')
	message = 'Subject: {}\n\n{}'.format(SUBJECT, TEXT)
	smtpObj.sendmail(From, To, message)
	smtpObj.close()
        print "Successfully sent email"
    except Exception as e:
        print(e)
            
    emit('forgotPassword')
    
@socketio.on('resetPassword', namespace='/login') 
def resetPassword(payload):

    
    if payload['accessCode'] == globalDict['accessCode']:
        globalDict['accessCode'] = ''
        message = {'success' : 'Access code accepted!\n'}
        emit('resetPassword', message)
        
    else:    
        message = {'error' : 'Access code denied!\n'}
        emit('resetPassword', message)
    
@socketio.on('updatePassword', namespace='/login') 
def updatePassword(payload):
    print("Payload: %s", payload)
    newpass = payload['password']
    updatePasswordQuery = "UPDATE login SET password=crypt(%s, gen_salt('bf')) WHERE passwordid = 1"
    write_query_db(updatePasswordQuery, (payload['password'], ))
    
    emit('updatePassword')
    
#################################    
# Retrieve School and School Divisions for forms

selectSchoolDivisions = "SELECT schoolDivisions.divisionName, ARRAY_AGG(schools.schoolName) FROM \
        schoolDivisions JOIN schools ON schoolDivisions.divisionId = schools.divisionId \
        GROUP BY schoolDivisions.divisionName ORDER BY schoolDivisions.divisionName"

@socketio.on('getDivisions', namespace='/student')
def getDivisionsForStudent():
    divisions = select_query_db(selectSchoolDivisions)
    emit("retrievedDivisions", divisions)
    
@socketio.on('getDivisions', namespace='/teacher')
def getDivisionsForTeacher():
    divisions = select_query_db(selectSchoolDivisions)
    emit("retrievedDivisions", divisions)
    
@socketio.on('getDivisions', namespace='/practica')
def getDivisionsForPractica():
    divisions = select_query_db(selectSchoolDivisions)
    emit("retrievedDivisions", divisions)

@socketio.on('getDivisions', namespace='/reports')
def getDivisionsForReports():
    divisions = select_query_db(selectSchoolDivisions)
    emit("retrievedDivisions", divisions)

######################################################
# Retrieve Practicum Bearing Courses for forms

selectPracticumCourses = "SELECT practicumCourses.courseName FROM practicumCourses"

@socketio.on('getPracticumBearing', namespace='/student')
def getPracticumBearingForStudent():
    courses = select_query_db(selectPracticumCourses)
    emit("retrievedPracticumBearing", courses)
    
@socketio.on('getPracticumBearing', namespace='/practica')
def getPracticumBearingForAssignment():
    courses = select_query_db(selectPracticumCourses)
    emit("retrievedPracticumBearing", courses)

@socketio.on('getPracticumBearing', namespace='/reports')
def getPracticumBearingForReports():
    courses = select_query_db(selectPracticumCourses)
    emit("retrievedPracticumBearing", courses)
    
##########################################################
# Submitting Information

@socketio.on('submitPractica', namespace='/practica')
def submitPractica(assignment):
    submit_practica(assignment)

@socketio.on('submitTransportation', namespace='/practica')
def submitTransportation(assignment):
    insertTransportationQuery = "INSERT INTO transportation(driverEmail, passengerEmail)  VALUES (%s, %s) RETURNING passengeremail"
    deleteTransporationQuery = "DELETE FROM transportation WHERE driverEmail = %s"
    
    driverEmail = assignment['driver']['email']
    error = delete_query_db(deleteTransporationQuery, driverEmail)
    
    for rider in assignment['passengers']:
        riderEmail = rider['email']
        result = write_query_db(insertTransportationQuery, (driverEmail, riderEmail), True)

@socketio.on('submit', namespace='/student')
def submitStudent(data):
    error = False
    msg = ''
    try:
        submit_student(data)
        msg = "Your information has been submitted!"
    except Exception as e:
        print(e)
        msg = "There was an error submitting your information. Try again."
    
    emit("submissionResult", {"error": error, "msg": msg})
    

@socketio.on('submit', namespace='/teacher')
def submitTeacher(data):
    error = False
    msg = ''
    try:
        submit_teacher(data)
        msg = "Your information has been submitted!"
    except Exception as e:
        print(e)
        msg = "There was an error submitting your information. Try again."
    
    emit("submissionResult", {"error": error, "msg": msg})

#################################  
# Loading Resources

@socketio.on('loadStudents', namespace='/practica') 
def loadStudents():
    students = load_students()
    emit('loadStudents', students)

@socketio.on('loadTeachers', namespace='/practica') 
def loadTeachers():
    teachers = load_teachers()
    emit('loadTeachers', teachers)
    
@socketio.on('loadPractica', namespace='/practica')
def loadPractica():
    practica = load_practica()
    emit('loadPractica', practica)

@socketio.on('loadTransportation', namespace='/practica') 
def loadTransportation():
    transportation = load_transportation()
    emit('loadTransportation', transportation)
    
#################################################
# Deleting Resources

@socketio.on('deleteTeacher', namespace='/practica') 
def deleteTeacher(teachId):
    deleteTeacherQuery = "DELETE FROM teachers WHERE teacherID=%s;"
    error = delete_query_db(deleteTeacherQuery, teachId)
    emit("deletedTeacher", error)
    
@socketio.on('deletePracticum', namespace='/practica')
def deletePracticum(pracId):
    print(pracId)
    deletePracticumQuery = "DELETE FROM practicumArrangement WHERE practicum=%s;"
    error = delete_query_db(deletePracticumQuery, pracId)
    emit("deletedPracticum", error)
    
@socketio.on('deleteTransportation', namespace='/practica') 
def deleteTransportation(driverEmail):
    deleteTransporationQuery = "DELETE FROM transportation WHERE driverEmail=%s;"
    error = delete_query_db(deleteTransporationQuery, driverEmail)
    emit("deletedTransportation", error)
    
################################# 
# Reports Section
   
@socketio.on('createReport', namespace='/reports')
def createReport(reportType, limit):
    
    directory = os.path.dirname(__file__)
    archivedReports = os.path.join(directory, 'static', 'archived_reports')
    
    if reportType == "school":
        filename = os.path.join(directory, 'static', 'reports', 'schoolreport.xlsx')
        rp.create_school_report(limit, filename)
        
    elif reportType == "division":
        filename = os.path.join(directory, 'static', 'reports', 'divisionreport.xlsx')
        rp.create_schoolDivision_report(limit, filename)
        
    elif reportType == "course":
        filename = os.path.join(directory, 'static', 'reports', 'coursereport.xlsx')
        rp.create_course_report(limit, filename)
        
        ########################################
        # zipName, zipPath = rp.batch_reports()
        # zipName = os.path.join(archivedReports, zipName)
        # print(zipName, zipPath)
        # shutil.make_archive(zipName, 'zip', zipPath)
        # shutil.rmtree(zipPath, ignore_errors=True)
        ########################################
    elif reportType == "transportation":
        filename = os.path.join(directory, 'static', 'reports', 'transportationreport.xlsx')
        rp.create_transportation_report(filename)
        
    emit("reportCreated", reportType)
        
@app.route('/reports/<reportType>')
def downloadReport(reportType):
    
    if reportType == "school":
        filename = "schoolreport.xlsx"
        
    elif reportType == "division":
        filename = "divisionreport.xlsx"
        
    elif reportType == "course":
        filename = "coursereport.xlsx"
    
    elif reportType == "transportation":
        filename = "transportationreport.xlsx"
        
    else:
        print("invalid report type")
        
    return send_from_directory(app.static_folder + "/reports", filename, as_attachment=True, 
    mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")

@socketio.on('getArchives', namespace='/reports')
def getDownloads():
    directory = os.path.dirname(__file__)
    archivedReports = os.path.join(directory, 'static', 'archived_reports')
    files = []
    
    for root, dirs, localfiles in os.walk(archivedReports, topdown=False):
            for name in localfiles:
                files.append(name)
            
    payload = {'files' : files}
    emit("listArchives", payload)

@app.route('/archived_reports/<file>')
def downloadArchivedReport(file):
    return send_from_directory(app.static_folder + "/archived_reports", file, as_attachment=True,
    mimetype="application/zip")

@socketio.on("deleteReport", namespace="/reports")
def deleteReport():
    directory = os.path.dirname(__file__)
    
    if os.path.isfile(os.path.join(directory, 'static', 'reports', 'coursereport.xlsx')):
        os.remove(os.path.join(directory, 'static', 'reports', 'coursereport.xlsx'))
        
    if os.path.isfile(os.path.join(directory, 'static', 'reports', 'schoolreport.xlsx')):
        os.remove(os.path.join(directory, 'static', 'reports', 'schoolreport.xlsx'))
        
    if os.path.isfile(os.path.join(directory, 'static', 'reports', 'divisionreport.xlsx')):
        os.remove(os.path.join(directory, 'static', 'reports', 'divisionreport.xlsx'))
    
    if os.path.isfile(os.path.join(directory, 'static', 'reports', 'transportationreport.xlsx')):
        os.remove(os.path.join(directory, 'static', 'reports', 'transportationreport.xlsx'))

######################################
# Running Application

        
@socketio.on('archSem', namespace="/reports")
def archSem(sem):
    directory = os.path.dirname(__file__)
    archivedReports = os.path.join(directory, 'static', 'archived_reports')
    successfulArchive = {}
    successfulArchive = archiveSemester(sem, archivedReports)
    print(successfulArchive)
    if successfulArchive['failure'] == False:
        print(successfulArchive['message'])
        print("emitting from server success")
        print({'semester': sem, 'success': "true"})
        emit('semesterArchived', {'semester': sem, 'success': "true" })
    else:
        print(successfulArchive['message'])
        print("emitting from server failure")
        emit('semesterArchived', {'semester':sem, 'success': "false"})

if __name__ == '__main__':
    socketio.run(app, host=os.getenv('IP', '0.0.0.0'), port=int(os.getenv('PORT', 80)), debug=True)
