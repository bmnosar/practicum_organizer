INSERT INTO students(email, firstName, lastName, hasCar, passengers) VALUES ('granger@hogwarts.uk', 'Hermione', 'Granger', false, 0);
INSERT INTO endorsements(endorsementName, studentemail) VALUES ('English', 'granger@hogwarts.uk');
INSERT INTO previousPractica(school,grade,course,studentEmail) VALUES ('Hugh Mercer Elementary School', 3, '', 'granger@hogwarts.uk');
INSERT INTO enrolledCourses(courseName,studentEmail) VALUES ('EDCI 502', 'granger@hogwarts.uk');
INSERT INTO meetingdays(monday, tuesday, wednesday, thursday, friday) VALUES (true, false, true, false, false) RETURNING meetingid;
INSERT INTO availabletimes (starttime, endtime, meetingid, studentemail) VALUES ('7:30AM', '3:30PM', 1, 'granger@hogwarts.uk');

INSERT INTO students(email, firstName, lastName, hasCar, passengers) VALUES ('potter@hogwarts.uk', 'Harry', 'Potter', true, 3);
INSERT INTO endorsements(endorsementName, studentemail) VALUES ('History & Social Sciences', 'potter@hogwarts.uk');
INSERT INTO enrolledCourses(courseName,studentEmail) VALUES ('EDCI 507', 'potter@hogwarts.uk');
INSERT INTO meetingdays(monday, tuesday, wednesday, thursday, friday) VALUES (true, false, true, false, false) RETURNING meetingid;
INSERT INTO availabletimes (starttime, endtime, meetingid, studentemail) VALUES ('7:30AM', '3:30PM', 2, 'potter@hogwarts.uk');

INSERT INTO teachers(email, firstname, lastname, hostSpring, hostFall, grade, schoolid, divisionid) VALUES ('mcgonagall@hogwarts.uk','Minerva','McGonagall',true,true,'5',1,1) RETURNING teacherid;
INSERT INTO meetingdays(monday, tuesday, wednesday, thursday, friday) VALUES ('True', 'True', 'True', 'True', 'True') RETURNING meetingid;
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('Writing','7:30AM','8:30AM',1,1,3);
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('Reading','8:30AM','9:30AM',1,1,3);
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('Math','9:30AM','10:30AM',1,1,3);
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('Social Studies','11:30AM','12:30PM',1,1,3);
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('Science','2:00PM','3:00PM',1,1,3);
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('Lunch','10:30AM','11:30AM',1,1,3);
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('Recess','12:30PM','1:00PM',1,1,3);
INSERT INTO meetingdays(monday, tuesday, wednesday, thursday, friday) VALUES ('True', 'False', 'False', 'False', 'False') RETURNING meetingid;
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('Art','1:00PM','2:00PM',1,1,4);
INSERT INTO meetingdays(monday, tuesday, wednesday, thursday, friday) VALUES ('False', 'True', 'False', 'False', 'False') RETURNING meetingid;
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('Computer','1:00PM','2:00PM',1,1,5);
INSERT INTO meetingdays(monday, tuesday, wednesday, thursday, friday) VALUES ('False', 'False', 'True', 'False', 'False') RETURNING meetingid;
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('Library','1:00PM','2:00PM',1,1,6);
INSERT INTO meetingdays(monday, tuesday, wednesday, thursday, friday) VALUES ('False', 'False', 'False', 'True', 'False') RETURNING meetingid;
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('Music','1:00PM','2:00PM',1,1,7);
INSERT INTO meetingdays(monday, tuesday, wednesday, thursday, friday) VALUES ('False', 'False', 'False', 'False', 'True') RETURNING meetingid;
INSERT INTO elementarySchedule(course, startTime, endTime, teacherID, schoolID, meetingID) VALUES ('P.E.','1:00PM','2:00PM',1,1,8);


INSERT INTO teachers(email, firstname, lastname, hostSpring, hostFall, grade, schoolid, divisionid) VALUES ('snape@hogwarts.uk','Severus','Snape',true,true,'',4,1) RETURNING teacherid;
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (9,'Lunch','12:30PM','1:30PM',2,4,'A/X');
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (9,'Lunch','12:30PM','1:30PM',2,4,'B/Y');
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (0,'Planning','11:30AM','12:30PM',2,4,'A/X');
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (0,'Planning','11:30AM','12:30PM',2,4,'B/Y');
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (1,'United States History: 1865 to the Present','7:30AM','9:30AM',2,4,'A/X');
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (2,'World History 1500 AD (C.E.) to Present','9:30AM','11:30AM',2,4,'A/X');
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (3,'World History and Geography to 1500 AD (C.E.)','1:30PM','2:30PM',2,4,'A/X');
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (4,'Virginia and United States History','2:30PM','3:30PM',2,4,'A/X');
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (1,'United States History: 1865 to the Present','7:30AM','9:30AM',2,4,'B/Y');
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (2,'Virginia and United States History','9:30AM','11:30AM',2,4,'B/Y');
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (3,'World History 1500 AD (C.E.) to Present','1:30PM','2:30PM',2,4,'B/Y');
INSERT INTO middleSchoolSchedule(block,course,startTime,endTime,teacherID,schoolID,dayType) VALUES (4,'World History and Geography to 1500 AD (C.E.)','2:30PM','3:30PM',2,4,'B/Y');